# MiniX10 - Gruppe 1
<!--blank line-->
Her kan du se den valgte [kode;](https://learn.ml5js.org/#/reference/image-classifier )

![](/MiniX10/miniX10.1.png)

![](/MiniX10/miniX10.2.png)

## Hvilken eksempel kode har I valgt, og hvorfor?

<!--blank line-->

Vi har valgt den kode som er kaldet ObjectDetector. Dette er et webcam baseret program,  som bruger billedklassificering. Den identificerer flere objekter og som deres placering i billeder eller video ved at tegne afgrænsningsfelter omkring det det den dekterer. Den bruger to forud trænede objektdetektionmetoder: YOLO og COCO-SSD. Ved at give programmet et billede eller en video, returnerer den en række navne, som indeholder klassenavne, afgrænsningsfelter og sandsynligheder af hvad der bliver vist. 

Vi synes det var et lidt sjovt program, og vi har også stødt ind i flere af disse slags programmer, som har samme slags billedklassificering, som man også kan bruge i hverdagen. Det er også spændende hvordan den nemt kan identificere nogle ting, og andre slet ikke, hvilket er noget vi legede lidt med. Det er også ret interessant, hvordan en kode, som er så kort, kan identificerer så mange forskellige ting. 


## Har I ændret noget i koden for at forstå programmet? Hvilke?
<!--blank line-->

Da koden i sig selv er meget kort, og det meste er meget essentielt for at koden virker. Derfor var det eneste vi kunne lave om i det visuelle. Vi legede lidt med at lave firkanten der omridser objekterne om til en cirkel, at lave StrokeWeight tyndere og tykkere, samt at ændre farven på teksten.  

## Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor?
<!--blank line-->

Først og fremmest var koden rigtig interessant, da det var den eneste kode der fungerede for os efter at have prøvet adskillige andre. Derudover fandt vi koden og programmet spændende, da vi læste om 'convolutional neutral network' (CNN) inde på ObjectDetector-hjemmesiden, hvilket er det, koden gør brug af. CNN er en type kunstigt neuralt netværk designet til at behandle data, der har en ‘gitterlignende’ struktur, såsom et billede. CNN'er bruges almindeligvis i billed- og videogenkendelse, naturlig sprogbehandling og andre opgaver, der involverer analyse af komplekse rumlige relationer og til sidst opdager indviklede mønstre, som den forudsiger hører til en bestemt kategori (fx person eller skab (som desværre blev identificeret som et køleskab)).

## Hvordan ville I udforske/udnytte begrænsningerne af eksempelkoden eller machine learning algoritmer?
<!--blank line-->

For at se hvilke begrænsninger som koden har, har vi prøvet at tjekke hvordan den reagerede ved forskellige objekter. På nogle af de billeder vi har taget af det, kan man også se hvordan den troede en kommode var et køleskab, dog kun når der var en person i billedet. Så det kom også an på hvilke andre objekter der kunne findes på billedet. Derudover ville man måske også kunne se hvad der skete, når man viste mennesker med forskellige hudfarver, for at se hvordan den reagerede på dette. 

## Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?
<!--blank line-->
Vi faldt over begrebet COCO-SSD som står for "Common Objects in Context - Single Shot Multibox Detector"(The coding train - ml5.js: Object Detection with COCO-SSD). 

## Hvordan kan I se en større relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen (fx. kreative AI, Voice Assistance, selvkørende biler, bots, ansigtsgenkendelse osv.)?
<!--blank line-->
Denne slags kode, som kan genkende forskellige ting og objekter, er generelt meget brugbare i nutiden til mange forskellige opfindelser, f.eks. ansigtsgenkendelse, bots, og de selvkørende biler vil nok også bruge det når de bliver færdig opfundet. I forhold til de selvkørende biler, som er en af de opfindelser der kommer til at påvirke samfundet meget i den nærmeste fremtid, og her kommer objektgenkendelse til at have stor indflydelse, og det er også nødvendig at det fungerer, for at bilen også fungerer, og ikke kører ind i nogle eller noget. 

## Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?
<!--blank line-->
Vi kunne godt tænke os at blive klogere på CNN og hvordan det fungerer? Fungerer det som en class eller en .JSON-fil? Vi ved at CNN er en type kunstigt neuralt netværk designet til at behandle data, men vi gad godt at vide, hvor denne form for data ligger gemt? Vi har kigget på Plain Javascript på hjemmesiden for ObjectDetector, hvor vi kan se der ligger billeder af henholdsvis katte og skildpadder, vi kan ikke finde køleskab eller personer, hvilket synes at være lidt skørt. 

Det er også spændende at undersøge de forskellige kategorier, og hvordan den inddeler objekterne, altså hvordan den definerer f.eks. hvad et menneske er. 


## Gruppe 1 GITLAB
<!--blank line-->

Camilla; https://gitlab.com/cami601k/aestetiskprogrammering

Kathrine: https://gitlab.com/kathrine2407/aestetisk-programmering

Thea H; https://gitlab.com/thea.heskjaer/aestetisk-programmering

Rikke; https://gitlab.com/RikkeOsmann/aesthetic-programming

Thea U; https://gitlab.com/theauhd/aestetiskprogrammering


