//Bevægelse øjne smiley 1
let venstreværdi = 135;
let højreværdi = 155;
let xværdi=250;
let yværdi=265;

//Bevægelse øjne smiley 2;
let mindstpunkt = 220;
let højesteværdi = 280;

function setup() {
  // 
  createCanvas(windowWidth, windowHeight);

}


function draw() {
  // 
  noStroke();
//backgrund
background(51,0,102)


noStroke();
//ansigtet smiley 1
fill(102,204,51);
ellipse(200,300,300,300);

//Hvide i øjet, smiley 1
fill(255)
ellipse(150, 270, 80, 120);

fill(255)
ellipse(260, 270, 80, 120);

//horn i  panden, smiley 1
fill(255,204,153)
triangle(90, 200,100, 110, 140, 165);

fill(204,153,102)
triangle(150, 160,200, 90, 250, 160);

fill(255,204,153)
triangle(280, 175,300, 110, 320, 210);

//mund, smiley 1
fill(102,0,0)
arc(200, 390, 100, 70, 0, PI);

//tunge, smiley 1
fill(204,102,102);
ellipse(200, 420, 40, 20);

//næse område , smiley 1
fill(51,102,51);
ellipse(200, 370, 150, 60);

//næsebore venstre, smiley 1
fill(0);
square(165, 350, 15, 10, 15, 10, 5);

//næsebore højre, smiley 1
fill(0);
square(210, 350, 15, 10, 15, 10, 5);


//kinder, smiley 1
fill(255,204,204);
ellipse(100, 340, 40, 35);

fill(255,204,204);
ellipse(310, 340, 40, 35);

//pupil, smiley 1
fill(0)
let venstrepupil = constrain(mouseX, venstreværdi, højreværdi);
ellipse(venstrepupil, 310, 40, 50);

let højrepupil = constrain(mouseX, xværdi, yværdi);
ellipse(højrepupil, 310, 40, 50);

  noFill();
  stroke(255)
  strokeWeight(10);
  ellipse(150, 270, 85, 120);
  ellipse(260, 270, 85, 120);

  noStroke();
//få den til at række tunge, smiley 1
if (mouseX>100 && mouseX<300 && mouseY>550 && mouseY<700){
  push()
  erase()
  ellipse(200, 420, 40, 20);
  noErase();

  if (mouseIsPressed){

  //tunge som er ude, smiley 1
    fill(204,102,102)
    arc(200, 410, 60, 80, 0, PI);

    //streg i tunge, smiley 1
 stroke(102,0,0);
 strokeWeight(2);
 line(200,410,200,435);

  pop();
  
  }
}


//Kasse under smiley 1
stroke(0)
fill(250)
rect(100,550, 200, 150)


//SMILEY 2

//ansigtet smiley 2 
noStroke()
fill(255,204,102);
ellipse(700, 300, 300, 300);


//Hvide i øjet venstre side, smiley 2)
fill(255)
ellipse(650, 260, 80, 120);


//hvide i øjet højre side, smiley 2)
fill(255)
ellipse(760, 260, 80, 120);

//pupil venstre side, smiley 2
fill(0)
let vpupil = constrain(mouseY, mindstpunkt, højesteværdi);
ellipse(650, vpupil, 40, 50);

//pupil højre side, smiley 2
fill(0)
let hpupil = constrain(mouseY, mindstpunkt, højesteværdi);
ellipse(760, hpupil, 40, 50);

//hvid prik i pupil højre side, smiley 2
fill(250)
let vprik = constrain(mouseY, mindstpunkt, højesteværdi);
ellipse(640, vprik, 10, 15);

//hvid prik i pupil højre side, smiley 2
fill(250)
let hprik = constrain(mouseY, mindstpunkt, højesteværdi);
ellipse(750, hprik, 10, 15);

//mund, smiley 2
stroke(0);
 strokeWeight(10);
 line(650,370,740,370);


////få den til at række tunge og ændre øjne, smiley 2
if (mouseX>600 && mouseX<800 && mouseY>550 && mouseY<700){
  push()
  fill(255,204,102);
noStroke();

  ellipse(650, 260, 80, 120);
  ellipse(650, 260, 80, 120);
  ellipse(650, 300, 40, 50);
  line(650,370,740,370);
  noErase();

  if (mouseIsPressed){

    //nye mund, smiley 2
    noStroke()
    fill(51,0,0)
arc(695, 370, 100, 80, 0, PI);

  //tunge som er ude, smiley 2
    fill(204,102,102)
    arc(695, 380, 60, 110, 0, PI);

    //streg i tunge, smiley 2
 stroke(102,0,0);
 strokeWeight(2);
 line(695,380,695,410);

 //ny venstre øje, smiley 2
 fill(51,0,0);
 arc(640, 280, 90, 70, PI, 0);


  pop();

  }
}


//Kasse under smiley 2
stroke(0)
fill(250)
rect(600,550, 200, 150)



}
