# MiniX2
<!--blank line-->
Her kan du se mit [program;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX2/index.html)

Her kan du se min [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX2/sketch.js)

![](/MiniX2/MiniX2.gif)


## Beskrivelsen af mit program

<!--blank line-->

Til denne MiniX2 skulle vi arbejde med Geometric emoji. Her lød opgaven på at designe to emojis. Til den opgave havde jeg ikke en klar plan eller overblik over, hvordan resultatet skulle se ud. Jeg vidste dog, at jeg ønskede at lave en mere eller mindre socialt og kulturelt neutralt emoji, og så en velkendt emoji, som vi selv møder i hverdagen. 

Her startede jeg med den nye emoji, som blev designet med små detajler, som pupiler, horn og meget mere. Derefter ønskede jeg den velkendte emoji til at have samme udtryk med mund og øjne.

Efter dette tilføjede jeg nogle effekter, så det for eksempel er muligt at trykke inde for de to firkanter, og så viser den ovenstående emoji et nyt ansigtsudtryk. Dette var nogle effekter, jeg ønskede at øve mig i og blive tryg i. 

Sidst tilføjede jeg effekt på pupilerne på emojierne, sådan det var muligt at styre dem ved hjælp af musen. 

## Hvad har jeg gjort brug af?
<!--blank line-->

Til mit program har jeg gjort brug af nogle forskellige koder. 

Til udformning af begge emoji gjorde jeg brug af forskellige geometriske former. Her gør jeg brug af;

  ellipse()
  arc()
  rect()
  triangle()

Dette er alle nogle geometriske former, som jeg har stiftet bekendtskab med tidligere, men jeg bestemt er blevet skarpere i efter dette program. Især placering af de forskellige elementer bliver bedre, da jeg har et bedre overblik over, hvor henne på programmet det cirka er. Dog finder jeg stadig arc() svær, så det er ikke en geometrisk form, som jeg føler mig helt sikker i. 

Derudover ønskede jeg en effekt, sådan at når musen trykkede i de pågældende firkanter, så vil der ske en ændring på emojien ovenfor. Til denne effekt gjorde jeg brug af et "if" statement.

      if (mouseX>100 && mouseX<300 && mouseY>550 && mouseY<700)

Her startede jeg med at definere området, som har betydning for, når der skulle ske en ændring. Derefter fik jeg tilføjet de ændringer under, som skulle ske, hvis if statementet blev opfyldt. Her tilføjes jeg også, at musen skulle være trykket nede. Sådan musen altså skulle være indenfor firkanten OG være trykket ned. 

Sidst ønskede jeg, at pupilerne skulle kunne bevæge sig i takt med musen. Derfor gjorde jeg brug af; constrain

       let venstrepupil = constrain(mouseX, venstreværdi, højreværdi);

Her definerede jeg venstre og højreværdi, som er lige værdierne som pupilerne skulle bevæge sig i mellem, når musen kører i x-aksen. På den anden emoji lavede jeg det samme, dog i forhold til y-aksen, sådan at pupilerne ryger op og ned i takt med at du rykker musen op og ned. 


## Den læste litteraturs påvirkning

<!--blank line-->

Den læste litteratur har skabt nogle tanker hos mig, om at der ligger mange tanker bag en emoji. En emoji er ikke bare en emoji. 

Når man prøver at repræsentere noget, risikerer man at ekskludere og støde andre, i forhold til køn, ligestilling, hudfarve, hårfarve og meget mere. Derudover man hurtig kommer til at lave universalism; sådan ser man ud, hvis man er ked af det for eksempel. 

Dette er helt klart nogle tanker, som jeg har taget med i mit program.

## Mit program i en socialt og kulturel kontekst
<!--blank line-->

I mit program har jeg skabt to emojier. Tanken bag dette var at prøve at skabe en forholdvis neutral (hvis den nogensinde kan blive det), hvor jeg har gjort brug af hudfarve, som ingen mennesker kunne spejle sig i, og andre funktioner som horn. 

Denne anden emoji er en emoji, som vi kender det, som vi er vant til at se. 

Begge emoji er forsøgt at have samme udtryk med øjnene og specielt tungen. 

Dette var egentlig et forsøg på at sammenligne og udfordrer vores tankegang med, hvordan vi forstår emoji, og hvad vi føler den skal indeholde. 

Et spørgsmål dertil kunne derfor være; om den venstre emoji repræsenterer det samme udtryk og mening, som den højre emoji, selvom den har effekter og farve, som ikke bliver forbundet med os mennesker?


## Hvad kunne opgraderes?
<!--blank line-->

Hvis man kigger på, hvad der kunne optimeres, så kunne man tilføje noget tekst, så man ikke var i tvivl, om man skulle trykke på i de hvide kasser. 

Samtidig også tilføje en form for information, sådan man vidste at man skulle bruge musen til at styre på pupilerne. Uden denne form for information til programmet kunne man godt risikere, at personen som kigger på det, misser de effekter. 

Derudover kunne man kigge på andre former for effekter, så den automatiske skiftede udtryk, og man ikke behøvede at skulle bruge musen til at trykke. 



