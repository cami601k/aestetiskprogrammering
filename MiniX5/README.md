# MiniX5
<!--blank line-->
Her kan du se mit [program;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX5/index.html)

Her kan du se min [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX5/sketch.js)

![](/MiniX5/MiniX5.gif)


## Beskrivelsen af mit program

<!--blank line-->

MiniX5 har været svær for mig, da jeg egentlig havde en ide, men som jeg måtte droppe på grund af nogle for komplekse syntakser og kodning. Derfor måtte jeg i tænkeboksen, da jeg stadig ønskede at beholde mit gitter til det generative program. 

Jeg kom derfor hurtigt på, at det skulle være nogle former for figurer, som skulle udfylde gitteret. Derfor lavede jeg en figur, som startede med at udfylde fra nederste venstre hjørne, som til sidst fyldte gitteret ud. 

I første omgang ønskede jeg, at det skulle være forskellige former for figure, som skulle være det generative og uforudsiglige for brugeren. Det blev dog for kompleks, da figurerne havde forskellige længder og bredde, og det ville derfor være for svært for programmet at aflæse, om der var plads til dem (dog ikke umuligt). Derfor blev det farverne på min figur, som blev det generative og udforsigelige i mit program. 

## Regler og processer
<!--blank line-->

I mit generative program har jeg gjort brug af en række regler. For eksempel gør jeg brug af forloops, hvor den ene fortæller programmeret, at den skal indsætte figuren indtil den når 14 figurer. En anden regel i programmet er et if-statement, som informerer programmet om, hvis x er større end bredden minus gitter_luften, så skal den rykke en række over. 

## Hvad har jeg gjort brug af?
<!--blank line-->

Til miniX5 programmet har jeg først og fremmest lavet et gitter, som er inspireret af gitteret af Winnie Soon. Dette er gjort ved at lave en function, hvor først kolonne og række (er defineret som global variabel) tildeles bredden/højden divideret gitter_Luft, som også er en global defineret variabel, som fortæller hvor store cellerne skal være i gitteret. Derefter definereres arr og tildeles et nyt array med kolonnen i. 

Derefter bruges der et forloop, hvor I først defineres og tildeles 0, og fortæller at i skal starte med at tælle fra 0. Derefter skal i være mindre end kolonne, det vil sige, at i tæller op til kolonne (som får tildelt en ny værdi efter hver forloop). Afslutning bliver der lagt 1 til i for hver gang loopet kører. Derefter laves der et nyt forloop, hvor der sker det samme med j, hvor at j skal være mindre række.

Efter forloopet defineres x og tildeles i ganges gitterluft, så i får en ny værdi efter hver forloop. Det samme sker med y, hvor j ganges med gitter_luft og får en ny værdi efter forloop. Derefter indsættes rect, som danner rammen af cellerne.  
~~~~
  function tegnGitter() {
  kolonne = floor (width/gitter_Luft);
  række = floor (height/gitter_Luft);
  let arr = new Array(kolonne); 
  for (let i = 0; i < kolonne; i++) { 
    arr[i] = new Array(række); 
    for (let j = 0; j < række; j++){ 
      let x = i * gitter_Luft; 
      let y = j * gitter_Luft; 
      stroke(0);
      strokeWeight(1);
      noFill();
      rect(x, y, gitter_Luft, gitter_Luft);
    }
  }

}
~~~~

Til at få figuren indsat på gitteret, gør jeg i første omgang brug af et forloop. I forloopet  definerer I først og tildeles 0, og fortæller at i skal starte med at tælle fra 0. Derefter skal i være mindre end number, det vil sige, at i tæller op til number (som får tildelt en ny værdi, dette kommer jeg videre ind på nedenfor). Derudover siger den, at i også er mindre 14, altså i tæller op til 14 (den generer maks 14 figurer på linjen). Afslutning bliver der lagt 1 til i for hver gang loopet kører. 

Herefter indhentes figuren og x tildeles til 3 gange gitter_Luft, sådan den nye figur indsætter efter den forgående figur

~~~~
  x=0
for (let i=0; i<number && i<14;i++){
  figur(x,y, gitter_Luft,gitter_Luft);
x+=3*gitter_Luft;
}
~~~~
 Derefter sætter tildeles number til number + 1 og ganges med deltaTime divideret med 11000, for at få den til at køre langsommere. Number er derfor stigende med 1 efter hver gang loopet har kørt. 
~~~~
number=number+1*(deltaTime/11000); 
~~~~

Derudover er der lavet et if-statement, som fortæller, at hvis x er større end bredden minus gitter_luften, så skal den sætte y til at være y-gitter-luft, så den rykker en række over, når den når hele bredden af skærmen. 

~~~~
if (x>width-gitter_Luft){
  y=y-gitter_Luft

} 

~~~~

Til programmet lavede jeg også min figur til en funktion. Her brugte jeg variablerne x og y, som er defineret global. Derudover går jeg brug af gitter_Luft, som egentlig gør, at jeg er helt sikker på, at figuren passer på gitteret, og hvis jeg ændrer gitter_Luft, vil selve figuren tilpasse sig dette. 

I forhold til fill, har jeg gjort brug af random, hvor den tager en tilfældig værdi/farve i intervalet 100 til 255 i R,G,B. 

~~~~
   function figur(x,y){
  fill(random(100, 255), random(100, 255), random(100, 255));
  rect(x, y, gitter_Luft, gitter_Luft);
  rect(x+gitter_Luft, y, gitter_Luft, gitter_Luft);
  rect(x+gitter_Luft+gitter_Luft, y, gitter_Luft, gitter_Luft);
}
~~~~

## Auto generator

<!--blank line-->

Generativ kunst er ikke et område, jeg har kendt til inden. Denne kunst er instruktionsbaseret, så kunstneren opstiller nogle regler eller instruktioner, som tilsammen skaber kunsten. Kunsten er med til at udfordre de tradionelle kunstformer, vi kender til og udforsker samtidig maskine kreativtet, som beskrives her: 

"This kind of approach is important, not only because it offers a different way of drawing and authoring works by machines based on mathematical logic, [it also provides] a sense of machine creativity that [...] negates intentionality and questions the centrality of human [...] agency.”(Soon & Cox, 2020)

Når man kommer ind på generativ kunst kan man også nævne randomness. Her kan man kigge på de forskellige regler eller instruktioner, som kunstneren tildeler, indenfor de regler kan der være noget randomness. Dette vil altså betyde, at kustneren ikke nødvendigvis vil kunne forestille sig den generativ kunst, da den vil kunne være forskellig gang efter gang. 

Dog kan man komme ind på true randomness, for computeren ikke har de egenskaber, der skal til for at vælge noget randomt, hvis der overhovedet er nogle som har de egenskaber? I stedet snakker man om Pseudo-randomness, hvor der bruges computerbaseret number-generator til at skabe de her tilfældigheder. 

I mit program har jeg også gjort brug af randomness i forhold til farverne på figurene, som indsættes. Her har jeg som kunstner ikke som sådan indflydelse på hvilken farve, som præcis tildeles den enkelte figur.  Dog kan man argumentere for, at jeg som kunstner alligevel har lidt indflydelse, da jeg har givet den et interval, hvor den skal vælge en tilfældig farve imellem. Det vil sige, at jeg ikke har nogle ide om, hvordan slutresultatet ser ud inden.

Alt i alt finder jeg generativ spændende og frusterende på samme måde. På en måde føler jeg tilfældighed og regler modsiger og er hinandens modsætninger. Jeg forbinder regler med struktur og overblik, hvor tilfældigheder er noget udvist og uforudsigligt. Dog er jeg stadig facionoeret af ideen, om at kunsten genererer noget nyt hvergang ud fra de samme regler og instruktioner. 

## Hvad kunne opgraderes?
<!--blank line-->

Der er helt klart nogle ting, som kunne opgraderes til en anden gang. For eksempel kunne mit ønske om forskellige figurer være en af tingene. Derudover kunne man også rette og indstille, sådan at den præcis kun kunne ligge den sidste figur, hvis det passede med gitteret. Lige nu ligger den figuren ovenpå gitteret, selvom der måske kun er en celle tilbage, og figuren fylder 3 celler. Så det er klart nogle ting, man kunne overveje at kigge på en anden gang. 

## Reference
<!--blank line-->

Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
