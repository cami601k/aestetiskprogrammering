
let gitter_Luft = 52; //så store, at ens felter/kasser bliver
let gitter = []; //array med gitter, on/off
//tegne kolonne og række
let kolonne, række;
let x;
let y;
let number = 0
let fr = 3;


function setup() {
  // vi skal lave et canvas, der fylder hele skærmen
  createCanvas(windowWidth, windowHeight)
  background(255);
  gitter = tegnGitter(); //Her indsættes gitterfunktionen
  x = 0 //Her sætter x til 0
  y = 15* gitter_Luft;
  frameRate(2); //hvor langsomt det kører
}

function draw() {

  for (let i = 0; i < number && i < 14; i++) {
    figur(x, y, gitter_Luft, gitter_Luft); //Her indhentes min figur
    x += 3 * gitter_Luft; //Her tildeles x til 3 gange gitter_Luft, sådan den nye figur indsætter efter den forgående figur.
  }
  number = number + 1 * (deltaTime / 11000);

  if (x > width - gitter_Luft) {
    y = y - gitter_Luft
    x = 0
  }


}

function tegnGitter() {
  kolonne = floor(width / gitter_Luft); //floor gør, at den afrunder til et helt tal, da arrays ikke kan regne med halve tal
  række = floor(height / gitter_Luft);
  let arr = new Array(kolonne); //Tager længden og dividerer med luft (linje 34) og finder ud af hvor mange antal "kolonner" i  arrays, der skal være
  for (let i = 0; i < kolonne; i++) { //forloop som laver kolonner
    arr[i] = new Array(række); //Tager højden og dividerer med luft (linje 35) og finder ud af hvor mange antal "rækker" i  arrays, der skal være
    for (let j = 0; j < række; j++) { //forloop som laver rækkerne
      let x = i * gitter_Luft; //den aktuelle x koordinat
      let y = j * gitter_Luft; //den aktuelle y koordinat
      stroke(0);
      strokeWeight(1);
      noFill();
      rect(x, y, gitter_Luft, gitter_Luft);

    }
  }

}

function figur(x, y) {
  fill(random(100, 255), random(100, 255), random(100, 255));
  rect(x, y, gitter_Luft, gitter_Luft);
  rect(x + gitter_Luft, y, gitter_Luft, gitter_Luft);
  rect(x + gitter_Luft + gitter_Luft, y, gitter_Luft, gitter_Luft);
}

