# MiniX3
<!--blank line-->
Her kan du se mit [program;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX3/index.html)

Her kan du se min [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX3/sketch.js)

![](/MiniX3/MiniX3.gif)


## Beskrivelsen af mit program

<!--blank line-->

MiniX3 gik ud på at skulle re-design og programmere en "aninimeret" throbber. Her skulle vi ved hjælp af looper og andre transformerende funktioner re-design en throbber. 

Opgaven var lidt svær for mig, fordi hvordan skulle jeg re-design et så simpelt design med så stærke relationer til de kendte og orginale throbber. Derfor havde jeg ikke et overblik over, hvordan throbberen skulle ende med at se ud. Jeg vidste dog, at jeg ville symbolisere tiden på en anden måde end den ellers traditionelle måde ved at bruge uret på en måde. 

Her startede jeg med at dannet fly ud forskellige former. Dette er en enkelt flyer uden de store detajler. Her gik det op for mig, at jeg ønskede at flyvere skulle dreje rundt i en cirkel og dannede selve throbberen. 

Derefter tilføjede jeg en jordklode i midten som centrum, hvor flyvmaskinen skulle danne throbberen rundt om. Ydermere tilføjes teksten, og røgen effter flyvmaskinen som lige informerer om cirklens omfang. Som den sidste detajle tilføjes jeg stjerner rundt omkring i programmet. 
 

## Hvad har jeg gjort brug af?
<!--blank line-->

Til miniX3 programmet har jeg gjort brug af forskellige syntakser. 

Til at designe og udforme flymaskinen og jordkloden er der gjort brug af forskellige geometriske former

  ellipse()
  quad()
  square()
  rect()
  vertex()

De geoometriske former ellipse og rect er jeg nogle former, som jeg har godt styr på og er skarp i dem. Udfordring lå derimod mere i formerne; quad, vertex og square. Dette er første gang, jeg bruger dem, og jeg brugte rigtig lang tid på at finde ud, hvordan de præcis virkede. Især vertex var svært, da man skaber sin egen figur ved at lave en masse punkter, som derefter bliver forbundet. 

I den her miniX gjorde jeg også brug af funktion for første gang, som er en funktion, som gjorde tingene en del lettere og mere overskueligt. Stjernene på himlen lavede jeg som en funktion, sådan jeg kunne bruge funktionen flere gange uden at skulle skrive alle de geometriske figur ind, hver gang stjernen skulle genereres. På den måde indsættes funktion makeStar flere gange op i draw med forskellige x,y værdier. 


      function makeStar (a,b) {
        fill(255,255,51)
    noStroke();
    beginShape();
 vertex(a+5, b+5);
 vertex(a, b+17.5);
 vertex(a+5, b+5);
 vertex(a+17.5, b);
 vertex(a+5, b-4);
 vertex(a, b-17.5);
 vertex(a-5, b-4);
 vertex(a-17.5, b);
 vertex(a-5, b+5);
 vertex(a, b+17.5);

 endShape();
    }

Flymaskinen blev også lavet som en funktion. Her gjorde jeg brug af translate(), så gør at koordinatsystemets nul punkt bliver rykket til et nyt punkt på canvas (det sted, som du tildeler den).

    translate(width/2, height/2);
   
 Derudover gjrode jeg brug af rotate i flymaskine funktionen, som får figuren til at dreje i en bestemt vinkel. 

    rotate(radians(cir));
  

## Temporalitet og throbbers

<!--blank line-->

Når man kigger på tid, så har vi alle sammen en forskellige opfattelse af tid. Udover det oplever vi, at maskiner har en helt anden forståelse, som ligger ude for menneskets. Maskinerne kigger helt enkel på, hvor lang tid noget tager, da den ikke har den menneskelige egenskab i at tænke over, hvor lang tid det føles i stedet for. Dette er en følelse, som jeg selv kender til. Nogle gange kan det føles som evigheder, hvor man har ventet på computeren, men ved et hurtig kig på tiden, så spiller den forestilling og den reel tid egentlig ikke sammen. Dette beskrives her; 

   "machine-time operates at a different register from human-time, further complicated by   global network infrastructures, and notions of real-time computation." (Soon & Cox,2020)

Her kommer throbber ind i billedet. Throbber er et symbol og udtryk for der loades noget, altså computeren tænker. Dette kan også ses som et symbol og udtryk for maskines usynlige arbejde, som man som menneske ikke får indblik i dette øjeblik. I mit program ønskede jeg med min throbber at udtrykke universet størrelse. Når jeg selv sidder og venter på noget, kan tiden føles udtrolig lang, selvom det måske højest er 1 min ventetid. Med den designede throbber ønskede jeg, at udtrykke tiden i universet. At den tid du venter, er intet i forhold til tiden i universet og på jorden. 

En throbber kan kommunikkere mange forskellige ting. Nogle kan være designet i forhold til tid, udførelsen eller brandet, som den er implemteret hos. 

## Hvad kunne opgraderes?
<!--blank line-->

Noget som kunne opgraderes, var i forhold til throbberen. Her kunne man lave selve flymaskinen i et større omfang, så man på den måde bedre kunne få vinklet flymaskinens bag-ende, sådan den også kører i ringen og ikke brød cirklen.

## Reference
<!--blank line-->
Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
