let x= 200
let y=200
let a;
let b;


function setup() {
  
  createCanvas(windowWidth, windowHeight);
// her bestemmes hvor hurtig/frame der skal være
  frameRate (2); 
  
 }
 
 function draw() {
   background(0,0,102);
   
   //her indsætter min funktion
   flymaskine();

   //her indsættes mine stjerner
   makeStar(200,300);
   makeStar(100,100);
   makeStar(400,50);
   makeStar(600,90);
   makeStar(800,120);
   makeStar(850,300);
   makeStar(650,300);
   makeStar(650,500);
   makeStar(700,700);
   makeStar(500,650);
   makeStar(500,650);
   makeStar(300,600);
   makeStar(150,670);
   makeStar(100,500);

   //jordkloden
   noStroke();
   fill(51,153,255)
ellipse(width/2,height/2,70);
fill(51,204,102)
ellipse(width/2+24,height/2+2,20,40);
square(width/2-15, height/2+10, 20, 10, 5, 10, 5);
rect(width/2-29, height/2-15, 10, 20);
quad(width/2-29, height/2-23, width/2-23, height/2-20, width/2-18, height/2-25, width/2-13, height/2-10);
square(width/2-26, height/2-25, 20, 10, 5, 10, 5);
  

//teksten
fill(255);
textSize(20);
textFont("Staatliches");
strokeWeight(5);
text("JEG TÆNKER LIGE....",width/2-80,height/2-160);

   

 }
 
 function flymaskine() {
   let num = -7;
   push();
   // Her midtercenterer vi min funktion
   translate(width/2, height/2);
   
// Her sætter jeg den til 360, sådan det bliver i en cirkel/ med num (som er antal figur) og * med frameCount % num igen
   let cir = 360/num*(frameCount%num);

   rotate(radians(cir));
  

   fill(250);
//flyet form
   ellipse(80, 40, 15, 100);
   //Bag vinger på fly
 quad(65, 80, 65, 70, 80,50,80,70 ,);
 quad(80, 70, 85, 55, 96,68,96,78 ,);
//for vinge på fly, venstre
 quad(50, 50, 50, 40, 80,15,80,35);
 //for vinge på fly, højre
 quad(80, 35, 80, 18, 110,35,110,45);

 //røg bagved
 fill(170);
 ellipse(75, -30, 10, 10);
 ellipse(65, -50, 10, 10);
 ellipse(45, -65, 10, 10);
 ellipse(20, -78, 10, 10);
 ellipse(-10, -78, 10, 10);
 ellipse(-40, -70, 10, 10);
 ellipse(-65, -50, 10, 10);
 ellipse(-78, -20, 10, 10);
 ellipse(-79, 12, 10, 10);
 ellipse(-79, 12, 10, 10);
 ellipse(-65, 45, 10, 10);
 ellipse(-40, 68, 10, 10);
 ellipse(-5,80, 10, 10);
 ellipse(30,73, 10, 10);

   pop();
   

 }


  function makeStar (a,b) {
    fill(255,255,51)
    noStroke();
    beginShape();
vertex(a+5, b+5);
vertex(a, b+17.5);
vertex(a+5, b+5);
vertex(a+17.5, b);
vertex(a+5, b-4);
vertex(a, b-17.5);
vertex(a-5, b-4);
vertex(a-17.5, b);
vertex(a-5, b+5);
vertex(a, b+17.5);

endShape();
    }


