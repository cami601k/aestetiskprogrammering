# Æstetiskprogrammering



## MiniX1

![](/MiniX1/minix1_screenshot.gif)

## MiniX2

![](/MiniX2/MiniX2.gif)


## MiniX3

![](/MiniX3/MiniX3.gif)

## MiniX4

![](/MiniX4/MiniX4.gif)

## MiniX5


![](/MiniX5/MiniX5.gif)


# MiniX6

![](/MiniX6/MiniX6.gif)

# MiniX7

![](/MiniX7/miniX7.gif)

# MiniX8

![](/MiniX8/FLOWCHART.MINIX.jpg)

# MiniX9

![](/MiniX9/minix9.gif)

# MiniX10

![](/MiniX10/miniX10.2.png)
