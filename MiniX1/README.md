# MiniX1
<!--blank line-->
Her kan du se mit [program;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX1/index.html)

Her kan du se min [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX1/sketch.js)

![](/MiniX1/minix1_screenshot.gif)


## Beskrivelsen af mit program

<!--blank line-->

Til min første MiniX1 bestemte jeg mig for at lave en panda ud af forskellige geometriske former, som til sidst udgjorde figuren. Udfordring her lå i, at finde de hele præcis x,y punkter, som var svært og udfordrende. Dog blev det lettere efter indsættelse af en masse geometriske former, og det gav en form for forståelse, hvad x og y cirka var på forskellige områder. 

Til baggrunden fik jeg inspiration til skyer fra Julie Sechers MiniX1, hvor hun havde brugt: 

circle ()

som var placeret i samme område, for at skabe den asymetriske form, som skyer har. 

Afslutningsvis lavede jeg en sol med solbriller på for at sætte prikken over i'et. Da dette var gjort, ønskede jeg at udfordre mig yderlig. Derfor bestemte jeg mig for at solen skulle bevæge sig. 

Dette løste jeg ved at lave x til en variabel. Sådan at figuren fortsatte ud af x-aksen.

Dette var ikke nogen nem opgave for mig, da jeg ønskede, at solbrillerne skulle rykke sig sammen med sol. Dette lykkedes til sidst ved at lave henholdsvis;

x, x1, x2, x3 som variable i de enkele former. 

## Min første oplevelse med kodning
<!--blank line-->

I mit program fokuserede jeg på at lave noget simple men kreativ. Denne tankegang blev dog hurtigt sløfret, da jeg blev fanget af at opgradere og videreudvikle. Dette endte i, at jeg endte med at bruge en masse koder/referencer, som egentlig ikke var tanken i første omgang. 

På den måde var første oplevelese med kodning positiv, da jeg hele tiden blev motiviteret til at forstå og udfordre mig selv med nye koder, som ved første øjenkast så indviklet ud.

Dog blev jeg også lidt for irrig, og jeg endte med at prøve nogle koder, som egentlig virkede, og gav noget visuelt, men som jeg egentlig ikke forstod. Det gik hurtig op for mig, at det var vigtigere for mig at forstå, hvad jeg skrev i stedet for at få et flot visuelt resultat. 


## Refleksioner omkring kodning fremfor læsning og skrivning
<!--blank line-->
Der var rigtig mange ting, som man skulle tænke over, når man laver sit program. Især skulle jeg hele tiden vende mig til tanken om, at programmet læser koderne oppefra og ned, og dette vil afspejle sig i koderne. Flere gange tog jeg mig selv i, at ikke kunne forstå, hvor mine former for eksempel var henne, og det vidste sig, at de var placeret bag et andet objekt på grund af rækkefølgen på min kode. Dette er klart en refleksion, som jeg skal være opmærksom på og tænke over i mit videre programmeringsarbejde. 

## Hvordan kan det læste litteratur påvirke?
Teksten "Coding Literacy" snakker om hvor vigtig, det er at kunne programmere og forstå sproget på et globalt plan. I teksten bliver begrebet; Illiteracy brugt i forhold til programmering, som er at man er ude af stand til at læse og skrive koder. Lige netop er det, jeg var inden semesterstart. Netop denne tekst giver noget til eftertanke, da man får øjene op for, hvor meget betydning og hvor stor del af programmering egentlig er af alles hverdag. Af den grund føler jeg det også essentielt at kunne forstå og skrive koder. 

## Hvad kunne opgraderes?
<!--blank line-->
Hvis jeg reflekterer over, hvad jeg kunne ændre og opgraderer på programmet, er det bestemt, at solen fortsætter tilbage på x-aksen og ikke forsvinder ud af canvas.

Dette skulle kunne lade sig gøre med;

if (x <= .....)

og af den grund er det helt klart et punkt, som kunne være interessandt at dykke længere ned i. 

Derudover kunne man kigge på, om man kunne få øjene til at blinke, eller få pupillerne til at køre rundt på selve pandaen. Her kan man kigge på nogle variabler, hvor man evt. kunne få musen til at styre, hvornår det sker. 

### Referencer
<!--blank line-->

Secher, Julie, MiniX1; 
https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX1/sketch.js

Annette Vee, Coding Literacty


# License

The p5.js library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 2.1.
