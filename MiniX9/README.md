# MiniX9 - Gruppe 1
<!--blank line-->
Her kan du se vores [program;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX9/index.html)

Her kan du se vores [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX9/sketch.js)

Her kan du se den ene JSON (outside) [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX9/outside.json)

Her kan du se den anden JSON (Myhead) [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX9/myHead.json)

![](/MiniX9/minix9.gif)

Programmet viser et sort hoved, hvor der forekommer negativt ladede indre tanker om et fiktivt individ. Teksterne og tankerne, der er udenom hovedet, er tanker som kommer udefra fra andre individer til "hoved"-individet, som er positive ladede tanker. 


## NAVNET PÅ VORES VÆRK

<!--blank line-->

‘My head’, ‘Self-Reflective Stories’, ‘In my head’
Disse navne fanger ideen om, at værket/jeg’et er både reflekterende og fiktive stemmer. De kan understrege oplevelsens introspektive karakter for brugeren af vores værk. Der kan være mange grunde til, at vi tænker dårligt om os selv. Det kan f.eks. være vores internaliserede negative overbevisninger eller budskaber fra andre, såsom ens forældre, jævnaldrende eller samfundet som helhed. 

Vi kan også hurtigt komme til at sammenligne os selv med andre, og på den måde føle, at vi kommer til kort på en eller anden måde. En medvirkende faktor for dette, kan skyldes dagens samfund, hvor sociale medier og andre former for medier konstant bombarderer os med billeder af tilsyneladende ‘perfekte’ mennesker, der lever et tilsyneladende ‘perfekt’ liv. 
Det er også muligt, at negativ tænkning er et resultat af en mental helbredstilstand såsom depression eller angst. Tilstande som kan give negative tanker og følelser om sig selv. 


## Hvordan virker det, og hvad har vi gjort brug af?
<!--blank line-->

Funktionen makeVisible() bliver brugt til at lave variabler som gør at teksten i json-filen kan blive vist. Funktionen indeholder et for-loop, som gør det muligt for teksten at forsvinde og ændre sig, så teksten bliver altså ved med at køre inde i hovedet i uendelighed. Funktionen udvendigSynlig() fungerer på samme måde, bare for teksten uden for hovedet.

JSON-file: I ‘The Coding Train’ forklarer Daniel Shiffman JSON-filer først ved at beskrive, hvad JSON er en forkortelse af, nemlig JavaScript Object Notation, og det er en måde at repræsentere data på, som er let for maskiner at læse og forstå, men som også kan være relativt let for mennesker at læse og og skrive. Det er en måde at repræsentere data som nøgle-værdi-par, hvor nøglen altid er en streng, og værdien kan være hvilken som helst type data, uanset om det er et tal, en boolean(true or false/conditional statements such as if statements or while), en streng, en matrix eller endda et andet JSON-object. Derudover fortsætter Shiffman med at forklare, at JSON ofte bruges til at udveksle data mellem en klient og server i webapplikationer, og at det er blevet en standard til dette formål. 


## Vocable Code og The Aesthetics of Generative Code
<!--blank line-->

Cox og McLean argumenterer for, at programmeringssprog i sig selv er en form for sprog, der ikke kun bruges til at instruere computere, men også til at kommunikere ideer mellem programmører og brugere. Nogle koder er lettere letlæselige end andre. Hvis man ser på vores kode kan den virke meget uoverskuelig og kaotisk, hvilket afspejler vores formål med programmet. De mange tanker som florerer dagligt inde i hovedet er også oftest kaotiske og ukontrollerbare. 

I teksten skrevet af Winnie Soon og co. bliver kode beskrevet som den ustabilitet, der er iboende i det menneskelige sprog med hensyn til, hvordan det udtrykker sig og fortolkes. Dette ses tydeligt i vores program, da der bliver skabt et æstetisk tiltalende og kunstnerisk udtryk. Derudover er det også en måde, hvor vi prøver at udtrykke vores idé, netop som Cox og Soon forklarer det, at vocable code ofte bruges i kreative kodnings-sammenhænge, hvor målet ikke bare er at producere et arbejdsprogram, men også at udtrykke en idé eller et koncept gennem selve koden. Tankerne om sig selv og de udefrakommende er meget simple i den forstand at teksterne har samme font og forekommer på samme vis på en generativ måde, med en bestemt random. 

På skærmen er der i princippet kun to forskellige elementer - hovedet og teksterne, som kan virke simplificeret, men eftersom at teksterne har bevægelse samt kommer og går, bliver der skabt en forvirring, som igen afspejler en form for tankemylder. 


## Gruppe 1 GITLAB
<!--blank line-->

Camilla; https://gitlab.com/cami601k/aestetiskprogrammering

Kathrine: https://gitlab.com/kathrine2407/aestetisk-programmering

Thea H; https://gitlab.com/thea.heskjaer/aestetisk-programmering

Rikke; https://gitlab.com/RikkeOsmann/aesthetic-programming

Thea U; https://gitlab.com/theauhd/aestetiskprogrammering

## Reference
<!--blank line-->
Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020

Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.

Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.


