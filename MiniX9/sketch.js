
let Camillahoved;
let andreHoved;
let insideThoughts = [];
let outsideThoughts = [];
let makingStatements=2
let speak;
let voices;
let queers;

function preload() {
  //Her loades begge JSON filer ind og tildeles et nyt navn
  Camillahoved = loadJSON('myHead.json');
  andreHoved = loadJSON('outside.json');

  //Her loades billedefilen ind og tildeles et navn
  mand = loadImage('mand.jpg');
}
//function som laves til at gøre de indvendige tanker synlige 
function indvendigSynlig() {
 
  //Her tildeles "tankeMylder", camillahoved (Jsonfilen). arrayet i filen
  tankeMylder = Camillahoved.mithoved;
 
  //Her bestemmes der hvor mange tanker som skal være på skærmen
  let addTanker = random(2,4);
 
  //Derefter laves der et forloop, hvor "i" først defineres og tildeles 0, og fortæller at i skal starte med at tælle fra 0. Derefter skal i være mindre end addTanker, som er et random tal mellem 2 og 4, det vil sige, at i tæller op til tallet. Afslutning bliver der lagt 1 til i for hver gang loopet kører.
  for (let i = 0; i <= addTanker; i++) {
   
    //Her defineres egnetanker, som bliver den valgte tanke fra arrayet. Her tager den et random tal fra arrayet længde i JSON .
    let egneTanker = int(random(tankeMylder.length));
    //Derefter putter den en ny i arrayet i sketchfilen
        insideThoughts.push(new notNew(tankeMylder[egneTanker].myhead));
  
  }
}

function udvendigSynlig() {
 //Her tildeles "snak", andreHoved (Jsonfilen). arrayet i filen
  snak = andreHoved.udenfor;
  //Her bestemmes der hvor mange tanker som skal være på skærmen
  let tilføjTanker = 6;

 //Derefter laves der et forloop, hvor indeks først defineres og tildeles 0, og fortæller at indeks skal starte med at tælle fra 0. Derefter skal indeks være mindre end tilføjtanker, som er 3, det vil sige, at indeks tæller op til tallet. Afslutning bliver der lagt 1 til indeks for hver gang loopet kører.
  for (let indeks = 0; indeks <= tilføjTanker; indeks++) {
    //Her defineres andretanker, som bliver den valgte tanke fra arrayet. Her tager den et random tal fra arrayet længde i JSON .
    let andreTanker = int(random(snak.length));
    
      //Derefter putter den en ny i arrayet i sketchfilen
        outsideThoughts.push(new ikkeNy(snak[andreTanker].udtryk));

  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(255);
  //Indsættelse af billede 
    imageMode(CENTER);
    image(mand, width / 2, height / 2);


   //Den kalder på de classes (worldwide og acts) 
  //og fortæller den at de skal virke på InsideThoughts 
  for (let badWords in insideThoughts) {
    insideThoughts[badWords].worldWide();
    insideThoughts[badWords].acts();


    //Når den så bliver showed (notFalse altså når den er true), så skal den så fjerne den igen når den når toppen
    let support = insideThoughts[badWords].shows();
    if (support == "notFalse") {
      insideThoughts.splice(badWords);

   }
  }

   //samme som før bare med outside 
   for (let goodWords in outsideThoughts) {
    outsideThoughts[goodWords].worldWide();
    outsideThoughts[goodWords].acts();

    let hjælp = outsideThoughts[goodWords].shows();
    if (hjælp == "notFalse") {
      outsideThoughts.splice(goodWords);

   }

  }
  //Denne if-statement tjekker hvor mange tanker der er tilbage på skærmen, hvis der er mindre end 3, så kalder den på funktionen, som genererer nogle nye
  if (insideThoughts.length <= 4) {
    indvendigSynlig();
  }

  if (outsideThoughts.length <= 2) {
   udvendigSynlig();
  }

}

//En form for class for selve teksten
function notNew(fåTanker) {
  //attributes of text
  //Tager en random værdi mellem 20 og 25)
  this.size = random(20, 25);

  this.time = random(1, 3);
  this.yyyyy = random(height/3.0, height+50);
  this.xxxxx = width/2;
  
  this.worldWide = function() {
   this.yyyyy-= this.time;
  
  };
  this.acts = function() {
    textSize(this.size);
    textAlign(CENTER);
    //Ingen streg rundt om teksten
    noStroke();
    fill(250);
    //Her tildeles de notNew og arrayet med positien som er defineret længere oppe) 
    text(fåTanker, this.xxxxx, this.yyyyy);
  };
  //Først definerer den status, bagefter siger den, hvis y postionen er mindre end 4 og større end højden + 10, så skal den sætte status til notFalse
  this.shows = function() {
    let status;
    if (this.yyyyy <= 4 || this.yyyyy >= height+10){
      status = "notFalse";
    }
    return status;
  };
}

function ikkeNy(snakOmDig) {
  //attributes of text
  //Tager en random værdi mellem 20 og 25)
  this.size = random(14, 20);

  this.time = random(1, 3);
  this.yyyyy = random(height/3.0, height+5);
  //vælger random mellem de to værdier
  this.xxxxx= random([200, 1200]);
  
  this.worldWide = function() {
   this.yyyyy-= this.time;
  
  };
  this.acts = function() {
    textSize(this.size);
    textAlign(CENTER);
    //Ingen streg rundt om teksten
    noStroke();
    fill(250,0,0);
    text(snakOmDig, this.xxxxx, this.yyyyy);
  };
  //check disappeared objects
 this.shows = function() {
    let status;
  if (this.yyyyy <= 4.34387 || this.yyyyy >= height+10.34387){
     status = "notFalse";
   }
    return status;
  };
}