let knap1;
let knap2;
let knap3;
let knap4;
let knap5;
let knap6;
let knap7;

//lav slider til en variabel
let slider;

//Lav de forskellige elementer til variabler
let element1;
let element2;
let element3;
let element4;
let element5;
let element6;
let element7;

//definer knapper, som vi skal bruge til vores "if" statment, sådan den bliver grå, når man har trykket på den
let knappen1;
let knappen2;
let knappen3;
let knappen4;
let knappen5;
let knappen6;


function setup() {
  // put setup code here
  createCanvas(800, 700);
  background(255,204,153);


  //række 1 = knap1 venstre, styling og placering//
  knap1 = createButton("Rød");
knap1.position(80,160);
knap1.size(70,25);
knap1.style("background-color", "#FFFFFF");
knap1.style("color", "#000000");
knap1.style("textStyle", "Staatliches");

//Her fortæller vi, at når knap1 trykkes ned, så er den true (skal bruge i min if-statement)
knap1.mousePressed(()  => element1 = true);

//Her fortæller vi, at når knap1 bliver frigivet, så er knappen 1 false 
//(skal bruges i if-statement, i forhold til at knappen bliver mørk, når den er markeret + at man kun kan vælge en knap i samme række)
knap1.mouseReleased(() => knappen1 = false);

//Der gøres det samme ved alle knapper

//række 1 = knap2 midten//
knap2 = createButton("Grøn");
knap2.position(180,160);
knap2.size(70,25);
knap2.style("background-color", "#FFFFFF");
knap2.style("color", "#000000");

knap2.mousePressed(()  => element2 = true);
knap2.mouseReleased(() => knappen2 = false);

//række 1 = knap3 højre//
knap3 = createButton("Blå");
knap3.position(280,160);
knap3.size(70,25);
knap3.style("background-color", "#FFFFFF");
knap3.style("color", "#000000");

knap3.mousePressed(()  => element3 = true);
knap3.mouseReleased(() => knappen3 = false);


//række 2 = knap4 venstre//
knap4 = createButton("🍎");
knap4.position(80,300);
knap4.size(70,25);
knap4.style("background-color", "#FFFFFF");
knap4.style("color", "#000000");

knap4.mousePressed(()  => element4 = true);
knap4.mouseReleased(() => knappen4 = false);


//række 2 = knap5 midten//
knap5 = createButton("🍋");
knap5.position(180,300);
knap5.size(70,25);
knap5.style("background-color", "#FFFFFF");
knap5.style("color", "#000000");

knap5.mousePressed(()  => element5 = true);
knap5.mouseReleased(() => knappen5 = false);

//række 2 = knap6 højre//
knap6 = createButton("🍉");
knap6.position(280,300);
knap6.size(70,25);
knap6.style("background-color", "#FFFFFF");
knap6.style("color", "#000000");

knap6.mousePressed(()  => element6 = true);
knap6.mouseReleased(() => knappen6 = false);

//Her laves slideren  med styling, mindste- og højesteværdi, 
//og hvor mange valgmuligheder den skal have (den indsættes i draw for at gøre den dynamisk)
slider = createSlider(0, 2, 2);
  slider.position(130, 470);
  slider.style('width','130px');
  slider.style('color','#FFFFFF');
 


//række 4 = endelige knap//
knap7 = createButton("Tegn for mig");
knap7.position(120,550);
knap7.size(160,25);
knap7.style("background-color", "#000000");
knap7.style("color", "#FFFFFF");

knap7.mousePressed(()  => element7 = true)

}

function draw() {
  // put drawing code here
  
  //indsættelse af slider, så man kan rykke med den, og den bliver interaktiv og dynamisk
  let val = slider.value();
  
  //lærred//
  stroke(102,51,0);
  strokeWeight(15)
  rect(400, 100, 350, 500);
  fill(250);
  rect(400, 100, 350, 500);

  //TEKST OVER LÆRRED//
  fill(0);
textSize(30);
textFont("Staatliches");
strokeWeight(1);
text("TEGN DIT PLAKAT",450,70);

  //TEKST OVER RÆKKE 1//
  fill(0);
textSize(18);
textFont("Staatliches");
strokeWeight(1);
text("DIN YNDLINGS FARVE?",120,130);

//TEKST OVER RÆKKE 2//
fill(0);
textSize(15);
textFont("Staatliches");
strokeWeight(1);
text("HVILKEN HAR DU MEST LYST TIL LIGE NU?",70,270);


//TEKST OVER RÆKKE 3//
fill(0);
textSize(15);
textFont("Staatliches");
strokeWeight(1);
text("DIT FORHOLD TIL SOMMER?",105,420);

//REFRESH tekst//
fill(255,102,51);
textSize(15);
textFont('Georgia');
strokeWeight(0);
text("REFRESH FOR AT STARTE FORFRA",450,650);


//TEKST TIL SLIDER VENSTRE SIDE//
fill(0);
textSize(30);
textFont("Staatliches");
strokeWeight(1);
text("😬",80,490);

//TEKST TIL SLIDER Højre SIDE//
fill(0);
textSize(30);
textFont("Staatliches");
strokeWeight(1);
text("😁",280,490);

//her laves et if-statement, som siger, at hvis knappen 1 er false (Det er den når du har trykket på knappen), //
//og de andre IKKE er falske, så skal den lave baggrundsfarven om. Sådan man kun kan vælge en af de tre muligheder //
if (knappen1 === false && knappen2 !== false && knappen3 !== false){
  knap1.style("background-color","#CCCCCC")
}
//Dette gøres med alle knapper i samme række//
if (knappen2 === false && knappen1!==false && knappen3 !==false){
  knap2.style("background-color","#CCCCCC")
}

if (knappen3 === false && knappen1 !==false && knappen2 !==false){
  knap3.style("background-color","#CCCCCC")
}

//her gøres det med række 2//
if (knappen4 === false && knappen5 !==false && knappen6 !==false){
  knap4.style("background-color","#CCCCCC")
}

if (knappen5 === false && knappen6 !=false && knappen4 !==false){
  knap5.style("background-color","#CCCCCC")
}

if (knappen6 === false && knappen5 !=false && knappen4 !==false){
  knap6.style("background-color","#CCCCCC")
}


//Her kommer alle de forskellige kombinationer, som danner de forskellige tegninger. TIP; lad vær med at læse dem alle... Men den siger, 
//at når en af de tre første knap og en af de 3 næste knapper i række to, 
//og når slideren har en værdi, som er enten; 0, 1 eller 2 og den endelig knap (knap 7) er trykket ned, så laver den den nedstående tegning.

if (element1 === true && element4 === true && val == 0 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);


}

if (element1 === true && element5 === true && val == 0 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}

if (element1 === true && element6 === true && val == 0 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}

if (element2 === true && element4 === true && val == 0 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}

if (element2 === true && element5 === true && val == 0 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}

if (element2 === true && element6 === true && val == 0 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}

if (element3 === true && element4 === true && val == 0 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}

if (element3 === true && element5 === true && val == 0 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}

if (element3 === true && element6 === true && val == 0 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(250);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(100);
text("❄",450,300);
textSize(50);
text("❄",650,350);
textSize(50);
text("❄",600,300);
textSize(50);
text("❄",650,250);
}




if (element1 === true && element4 === true && val == 1 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);

}

if (element2 === true && element4 === true && val == 1 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);

}

if (element3 === true && element4 === true && val == 1 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);
}

if (element1 === true && element5 === true && val == 1 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);
}

if (element2 === true && element5 === true && val == 1 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);
}

if (element3 === true && element5 === true && val == 1 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);
}


if (element1 === true && element6 === true && val == 1 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);
}

if (element2 === true && element6 === true && val == 1 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);
}

if (element3 === true && element6 === true && val == 1 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(102,204,255);
circle(680,160,70);
circle(590,160,70);
circle(640,140,70);
circle(630,170,70);
textSize(80);
text("💧",450,300);
textSize(50);
text("💧",650,350);
textSize(50);
text("💧",600,300);
textSize(50);
text("💧",650,250);
}

if (element1 === true && element4 === true && val == 2 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element2 === true && element4 === true && val == 2 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element3 === true && element4 === true && val == 2 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍎",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element1 === true && element5 === true && val == 2 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element2 === true && element5 === true && val == 2 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element3 === true && element5 === true && val == 2 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍋",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element1 === true && element6 === true && val == 2 && element7 === true ) {
  fill(255,153,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element2 === true && element6 === true && val == 2 && element7 === true ) {
  fill(102,255,153);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}

if (element3 === true && element6 === true && val == 2 && element7 === true ) {
  fill(0,255,255);
  rect(400, 100, 350, 500);
  textSize(200);
text("🍉",500,500);
noStroke();
fill(255,255,153);
textSize(180);
text("☀",550,260);
}


}
