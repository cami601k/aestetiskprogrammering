# MiniX4
<!--blank line-->
Her kan du se mit [program;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX4/index.html)

Her kan du se min [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX4/sketch.js)

![](/MiniX4/MiniX4.gif)


## Beskrivelsen af mit program

<!--blank line-->

I denne uge miniX vidste jeg, at jeg ville arbejde med DOM elementer og startede ud med at danne et lærred, som skulle være det visuelt output for brugeren, da jeg ønskede brugeren skulle have noget visuelt ud af, den data som var givet. Derfor lavede jeg forskellige knapper på venstre side med forskellige data og valgmuligheder, som kunne trykkes af. Nedenfor knapperne tilføjede jeg en slider, hvor man har mulighed for at selv visualisere noget på en skala udfra spørgsmål ovenfor. Afslutningvis er der en knap, som er den enedelig knap, som genererer den endelig kunst og plakat udfra det, som er valgt. Outputtet vil derefter vises på lærredet i højre side. 

## POSTER GENERATOR

<!--blank line-->

Til festivalen er der blevet udviklet et program, som kaldes "poster generator". Værket har til formål at visualisere noget tydeligt og konkret til datagiveren, da data har en tendens til at være en gråzone, hvor man som datagiver ikke ser formålet eller får noget ud af give data til datamodtagerne. Formålet er med dette værk at gøre data til kunst, noget positivt, og belyse at data også kan være kunstnerisk og skabe værdi for datagiveren også. 

## Hvad har jeg gjort brug af?
<!--blank line-->

Til miniX4 programmet har jeg udfordret mig meget

Jeg har gjort af DOM elementer, hvor jeg har brugt knap funktion, hvor jeg har tilføjet styling og position, og gjort knappen til en variabel 
~~~~
   knap1 = createButton("Rød");
   knap1.position(80,160);
    knap1.size(70,25);
   knap1.style("background-color", "#FFFFFF");
   knap1.style("color", "#000000");
   knap1.style("textStyle", "Staatliches");
~~~~

Til programmet lavede jeg også en slider, med styling, mindste- og højesteværdi, og hvor mange valgmuligheder den skal have. Slideren indsættes i også som en variabel i draw for at gøre den dynamisk og interaktiv. 

~~~~
   slider = createSlider(0, 2, 2);
   slider.position(130, 470);
   slider.style('width','130px');
   slider.style('color','#FFFFFF');
~~~~

Derudover har jeg gjort brug af en input via events. I første omgang har jeg gjort sådan, at når knappen bliver presset ned blev element1 true, dette er noget som skulle bruges til vores if statement.

~~~~
   knap1.mousePressed(()  => element1 = true);
~~~~

Her fortæller vi, at når knap1 bliver frigivet, så er knappen 1 false, som også skal bruges i if statement.
 ~~~~
   knap1.mouseReleased(() => knappen1 = false);
~~~~

Derefter har jeg lavet et if-statement, som siger, at hvis knappen er false
og de andre IKKE er false (altså ikke trykket på), så skal den lave baggrundsfarven om (så man visuelt kan se, at den er valgt) og sådan man kun kan vælge en af de tre muligheder i rækken
~~~~
   if (knappen1 === false && knappen2 !== false && knappen3 !== false){
   knap1.style("background-color","#CCCCCC")
}
~~~~

Sidst har jeg gjort brug af de if-statements og boolean, som laver plakatten på lærred. Her er der i alt 27 kombination, som er lavet. I forhold til nedenstående siger den, hvis knap1 er trykket ned, og knap4 er trykket ned og slideren er på mindsteværdien (0), og knap 7 (den endelige knap), så skal den tegne de nedestående geometriske former og text på lærredet. 
~~~~
   if (element1 === true && element4 === true && val == 0 && element7 === true ) {
   fill(255,153,153);
   rect(400, 100, 350, 500);
   textSize(200);
   text("🍎",500,500);
   noStroke();
   fill(250);
   circle(680,160,70);
   circle(590,160,70);
   circle(640,140,70);
   circle(630,170,70);
   textSize(100);
   text("❄",450,300);
   textSize(50);
   text("❄",650,350);
   textSize(50);
   text("❄",600,300);
   textSize(50);
   text("❄",650,250);
   ~~~~

Dette gøres ved alle 27 kombinationer ;)), det tog lang tid ja. 


## "Capture all og data capture"

<!--blank line-->

Data capture er for mig et svært emne og område. Der er ingen tvivl, om jeg er opmærksom på data capture, men for mig er det blevet en selvfølge og en normalitiet. Hver dag arbejder jeg med data, håndtere data og ikke mindst indsamle data, som bliver analyseret. 

"Datafication can be understood [] as a new mode of data colonialism (Mejias & couldry, 2019) that appropriates human lifte so that data can be continously extracted"

I ovenstående citat beskriver godt data capture. Man kan her snakke om den nye form for "arbejdsklasse", som hele tiden bliver overvåget af en "overklasse", og man har ikke mulighed for at strejke eller gå imod, for det bliver også gjort til data, som kan bruges. 

Af den grund er data capture og Capture all lidt en ond spiral, som vi som mennesker ikke kan se os ud af, som jeg finder dybt bekrymrende og uvirkelig.

Modsat vil mit arbejde den anden side ikke være det samme uden den form for oplysning og information, og mit arbejde vil være lidt en blindgyde og et gæt, da data capture for mig er en form for ledetråd og sti til, hvad jeg arbejder med. 

Data Capture er mange ting, og rigtige mange stiller kritisk til det. Derfor addreserer mit program noget af de mindre kritiske ved data capture, og gennemsigtighed i, hvad ens data for eksempel kunne bruges til. 

## Hvad kunne opgraderes?
<!--blank line-->

Noget som kunne opgraderes, er i forhold til det visuelle på lærred. Her kunne man måske også tænke noget bevægelse og rotation ind. Derudover er der lidt nogle mangler i programmet. Lige nu kan man ikke vælge en ny knap i rækken, når man allerede har valgt en, så det kunne være en opgradering, som man kunne kigge ind i. Sådan man ved et tryk kunne fjerne markering af knappen og vælge en ny knap i stedet for at refresh hele siden. 

## Reference
<!--blank line-->
Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021.
