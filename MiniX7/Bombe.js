class Bombe {
    constructor(){
        this.posX = width;
        this.posY= random(height);
        this.speed = floor(random(2,6));
    }

    move() {
        this.posX -= this.speed;
        if (this.posX<-60){
            this.posX=width+4;
            this.posY=random(height);
        }
    }

    show() {
        textSize(40);
        text("💣", this.posX, this.posY);
    }
}
