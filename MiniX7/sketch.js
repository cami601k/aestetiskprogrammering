let birdSize = {
  w: 95,
  h: 99
};

let bird;
let birdPosY;
//Array til æblerne
let æble = [];
let point = 0
//array til æblerne
let bombe = [];
let minBombe=4;
let start;
let knap6;

function preload() {
  //her preloader man billederne, som skal bruges længere nede og tildeler dem et navn
  baggrund = loadImage('baggrund.jpeg');
  fugl = loadImage('bird.gif');
  exsplosion = loadImage('exsplosion.png');
  pokal = loadImage('pokal.png');
  piletaster= loadImage('piletaster.png');
  soundFormats('mp3');
  bombeSprængt=loadSound("eksplosion.mp3");
  spiseÆble=loadSound("spiseÆble.mp3");
}

function setup() {
  createCanvas(1200, 800);
  //fuglen position tildeles højden dividereret med 2
  birdPosY = height / 2;
  æble.push(new Æble());
  bombe.push(new Bombe());

  knap6 = createButton("START");
  knap6.position(450, 600);
  knap6.size(200, 50);
  knap6.style("background-color", "#FFFFFF");
  knap6.style("color", "#000000");

  knap6.mousePressed(() => start = true);

}

function draw() {
  background(baggrund);
  //Her indsættes fuglen, som har birdPosY, som er defineret i setup, og så har den birdsize oppe fra globale
  image(fugl, 30, birdPosY, birdSize.w, birdSize.h);
  


//REGLER DISPLAY
fill(211,211,211,200);
rect(200,100,700,550);

fill(0);
textSize(40);
text("BIRD GAME", 450, 180)
fill(0);
rect(425,200,270,3);

textSize(20);
text("- Spis æblerne og undgå bomberne for at vinde!", 250, 300)
text("- Brug piletasterne op og ned for at styre fuglen", 250, 450)




image(piletaster,600,400);

//Ring rundt omkring piletasterne
push();
fill(0,15)
stroke(255,0,0);
strokeWeight(2);
ellipse(730,480,90,230);
pop();



  

  if(start===true){
    knap6.hide();
    background(baggrund);
    image(fugl, 30, birdPosY, birdSize.w, birdSize.h);
    startSpil(); 
   
  
}
}

function startSpil(){
  showÆble();
  showBombe();

//indsættelse af de forskellige funktioner, som er lavet nedenfor
  checkPoint();
  checkLose();
  checkWin();
  checkBombeNum()
  flereBomber();

  textAlign(CENTER);
  fill(250);
  text("Point: " + point, width / 2, 90)
  textSize(20);
  text("Spis æblerne og undvig bomberne", width / 2, 30)

//to if-statement som siger, at når frameCount er over 100, så skal den indsættes et nyt æble og en ny bombe
  if (frameCount % 100 == 0) {
    æble.push(new Æble());
  }


}

function showÆble() {
  for (let i = 0; i<æble.length; i++) {
    æble[i].move();
    æble[i].show();
    //her siger vi, at hvis æblet er ude af billedet, så skal den fjerne den fra arrayet
    if (æble[i].x < 0) {
      æble.splice(i, 1);
    }
  }
}

function showBombe() {
  for (let i = 0; i<bombe.length; i++) {
    bombe[i].move();
    bombe[i].show();
    //her siger vi, at hvis bomben er ude af billedet, så skal den fjerne den fra arrayet
    if (bombe[i].x < 0) {
      bombe.splice(i, 1);
    }
  }
}

function checkBombeNum(){
  if(bombe.length<minBombe){
bombe.push(new Bombe());

  }
}



function checkPoint() {
  let distance;
  for (let i = 0; i < æble.length; i++) { 
    //Her udregnes distancen vha. dist, som beregner mellem to punkter, som så er vores æble og fugl
    distance = dist(birdSize.w / 2, birdPosY + birdSize.h / 2, æble[i].posX, æble[i].posY);

    if (distance < birdSize.w / 2.5) { 
      //Her laves et if-statement, som registerer, 
      //hvis fuglen er tæt nok på æblet, så giver den et point og fjerner den fra arrayet
      point++;
      spiseÆble.play();
      æble.splice(i, 1);
    }
  }
}

function checkLose() {
  let distanceBombe;
  for (let i = 0; i < bombe.length; i++) {
     //Her udregnes distancen vha. dist, som beregner mellem to punkter, som så er vores bombe og fugl
    distanceBombe = dist(birdSize.w / 2, birdPosY + birdSize.h / 2, bombe[i].posX, bombe[i].posY);
    if (distanceBombe < birdSize.w / 2.5) {
      //Her laves et if-statement, som registerer, 
      //hvis fuglen er tæt nok på bombe, så laver den nogle effekter og stopper spillet. 
      fill(0);
      rect(0, 0, width, height);
      bombeSprængt.play();
      image(exsplosion, 90, 90);
      fill(0);
      textSize(40);
      text("...GAME OVER", width / 2, height / 2);
      noLoop();

      //Her laves der en knap, så kan være med til at reloade siden, så man kan prøve igen
      button = createButton('Prøv igen');
      button.position(width / 2 - 150, height / 2 + 100);
      button.style("background-color", "#333333")
      button.size(300, 50);
      button.style("color", "#FFFFFF");
      button.size('back');
      button.mousePressed(Prøvigen);

      //Function, som kan reload vinduet, så at når knappen er trykket ned, så vil den lave denne funktion
      function Prøvigen() {
        window.location.reload(true);
      }


    }
  }
}

function flereBomber(){
if (point >5){
  minBombe=7;
}

}


function checkWin() {
  //Funktion, som har if-statement, sådan den tjekker, om point er større eller lig med 9, 
  //så har du vundet spillet og det vil stoppe. 
  if (point >= 14) {
    fill(0);
    rect(0, 0, width, height);
    image(pokal, 250, 90);
    fill(250);
    textSize(40);
    text("YOU WIN! ", 515, 650);
    noLoop();
  }
}


//Funktion til at styre fuglen med op og ned tasten
function keyPressed() {

  if (keyCode === UP_ARROW) {
    birdPosY -= 50;
  } else if (keyCode === DOWN_ARROW) {
    birdPosY += 50;
  }
  //Her laves der et if-statement, som siger hvis y-positionen på fuglen er over 700, 
  //så skal den blive 700, sådan den aldrigi vil komme ud af canvaset 
  if (birdPosY > 700) {
    birdPosY = 700;
    //Det samme gøres her, sådan at hvis y-positionen på fuglen er mindre end 0 - (halvdelen af fuglen), 
    //så skal positionen være 0, så den heller aldrig kommer ud 
  } else if (birdPosY < 0 - birdSize.w / 2) {
    birdPosY = 0;
  }


}
