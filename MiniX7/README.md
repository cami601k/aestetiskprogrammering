# MiniX7
<!--blank line-->
Her kan du se mit [program;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX7/index.html)

Her kan du se min [kode;](https://cami601k.gitlab.io/aestetiskprogrammering/MiniX7/sketch.js)

![](/MiniX7/miniX7.gif)


## Beskrivelsen af valgt MiniX

<!--blank line-->

I denne uges miniX har jeg valgt at tage fat i miniX6 igen. Object-programmering har været svært for mig, og jeg har derfor brugt denne miniX til at forstå emnet bedre både i forhold til syntakser men også det reflektive med emnet. Gensynet med denne miniX har givet muligheden for at udvikle spillet med detaljer, startknap, lydeffekt og sværhedsgrader, som alt andet lige skulle forbedre oplevelsen for brugeren.

## Ændringer og forberedringer MINIX6
<!--blank line-->

Alt i alt har konceptet, spilreglerne og de faste rammer for spillet ikke ændret sig meget. Dog er der tilføjet nogle detaljer, knap og ting, som er med til overall give en bedre oplevelse for brugeren, og skabe en bedre sammenhæng og rød tråd. Et irrations momement ved spillet var, at det startede med det samme, at canvaset var loadet. Som bruger havde man så ikke mulighed for at forstå regler og vide, hvad man skulle gøre, inden man potenielt kunne ramme en bombe og dø. 


Dette problem løste jeg ved at lave en funktion, som startede spillet. Helt enkelt rykkedes jeg alt, jeg havde i draw og indsatte det i funktionen. Derefter tilføjede jeg en knap, og et display med regler. 

![](/MiniX7/MiniX7.2.png)

Til knappen tilføjede jeg, at når knappen var trykket ned, skullle den lave start true, som er en global variabel. I min draw tilføjede jeg afslutningvis et if-statement, som sagde, at hvis start var true, så skulle den gemme knappen, tilføje baggrund + fugl (for at gemme displayet) og gøre funktion Start Spil.

~~~~

 if(start===true){
    knap6.hide();
    background(baggrund);
    image(fugl, 30, birdPosY, birdSize.w, birdSize.h);
    startSpil(); 
   
~~~~

Derudover ønskede jeg i miniX6 at tilføje nogle lydeffekter til spillet for at øge spænding og tilføje lidt til historien. Dog må jeg give op på det der, men det er en forberedring jeg har fået til at virke nu, og jeg forstår nu.

~~~~
soundFormats('mp3');
  bombeSprængt=loadSound("eksplosion.mp3");
  spiseÆble=loadSound("spiseÆble.mp3");

~~~~

Dernæst ønskede jeg at øge sværhedsgraden, når man har nået til et vis punkt i spillet. Dette gjorde jeg ved hjælp af indsætte flere hajer, når point nåede over 5. Nogle tanker jeg havde gjort mig omkring det, var at jeg synes selv det er svært at holde fokus i et spil, hvis der ikke skete en form for udfordring, level op eller andet igennem spillet. På den måde motivierer man spilleren og på en måde fastholder man dem alt andet lige i længere tid. 

~~~~

function flereBomber(){
if (point >5){
  minBombe=7;
}

}

~~~~

![](/MiniX7/MiniX7.3.png)

## Videreudvikle inkorpering koncepter i henhold til litteraturen
<!--blank line-->

Som nævnt havde jeg valgt miniX6 for at få en bedre forståelse for både syntakserne, emnet og litteraturen i objekt-programmering. 

Gemmen denne miniX har jeg fået end bedre forståelse af Objektorientieret programmering og dens indhold og betydning. At man arbejder med objekter, og det ikke kun omhandler at disse objekter skal formes og ligne virkeligheden og verdens mest muligt, men også tydeliggøre de relationer, som er mellem mennesket og de objekter, og hvordan interaktionen er også. 

Derudover når man arbejder med objekt kan de arve noget subjektivitet og antagelse om verden. Her vil det resultere i en uetiske abstraktioner og repræsentation. Så på den måde kan ens objekter komme til at forstærke hvem der er ikke inkludere og repræsenteret.

Dette kan man perspektive til emoji og miniX2, hvor man også kunne komme ind på universalisme, og hvem der er inkluderet og repræsenteret og hvem der ikke er. 

## Relationen mellem æstetisk programmering og digital kultur

<!--blank line-->

Æstetisk programmering ses ikke kun som værende et emne til at forbedre problemløsning og analytiske færdigheder, men muligheden for at stille spørgsmål og undren ved eksisterende teknologiske paradigmer og skabe ændringer i det tekniske system. På den måde kan det anses som et dynamisk kulturet praksis og en måde hvornår man kan tænke på, og samtidig et redskab til at forstå komplekse proceduer og paradigmer, som udgør vores liv. 

_"Such practical “knowing” also points to the practice of “doing thinking,” embracing a plurality of ways of working with programming to explore the set of relations between writing, coding and thinking to imagine, create and propose “alternatives.”_ (Soon & Cox, 2020)



## Reference
<!--blank line-->
Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
