class Æble {
    constructor() {
        this.posX = width;
        this.posY= random(height);
        this.w = 80;
        this.speed = 3;
    }

    move() {
        this.posX -= this.speed;
    }

    show() {
        textSize(50);
        text("🍎", this.posX, this.posY);
    }
}
