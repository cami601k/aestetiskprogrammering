class Bombe {
    constructor(){
        this.posX = width;
        this.posY= random(height);
        this.speed = 3;
    }

    move() {
        this.posX -= this.speed;
    }

    show() {
        textSize(40);
        text("💣", this.posX, this.posY);
    }
}
